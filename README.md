# Сервис Reader

Сервис Reader осуществляет чтение и передачу данных с локальный машины на удаленный ресурс (сервис Writer).

## Работа с сервисом
Развертывание проекта
```bash
git clone https://gitlab.com/r-vision-test/reader.git
cd reader
npm install
npm run build
npm run start -- --nats --from './example' --to 'nats://localhost:4222/'
```

## Опции
### **--type**
Способ передачи данных.
* `--type web-socket` передача данных с использованием веб-сокета.
* `--type nats` передача данных с использованием NATS.
### **--web-socket** / **-ws**
Тоже, что `--type web-socket`
### **--nats**
Тоже, что `--type nats`
### **--from** / **-f**
Файл на локальной машине, который нужно передать.
### **--to** / **-t**
Адрес удаленного ресурса.

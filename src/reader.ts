import * as colors from "colors";
import * as fs from "fs";
import * as stream from "stream";
import RemoteWriteStream from "./streams/RemoteWriteStream";
import WebSocketWriteStream from "./streams/WebSocketWriteStream";
import parseCommandLineArgs from "./utils/parseCommandLineArgs";
import NatsWriteStream from "./streams/NatsWriteStream";

/**
 * Инициализирует Reader для передачи данных на удаленный ресурс.
 */
export default function reader(options: IReaderOptions = parseCommandLineArgs()) {
  const type = options.type;

  if (typeof type !== "string") {
    console.error(`${colors.red("[Error]")} Reader option 'type' must be a string.`);
    return;
  }

  let writeStreamClass: typeof RemoteWriteStream | undefined;

  if (type === "web-socket") {
    writeStreamClass = WebSocketWriteStream;
  } else if (type === "nats") {
    writeStreamClass = NatsWriteStream;
  } else {
    console.error(`${colors.red("[Error]")} Write stream is undefined.`);
    return;
  }

  const to = options.to;

  if (typeof to !== "string") {
    console.error(`${colors.red("[Error]")} Reader option 'to' must be a string.`);
    return;
  }

  writeStreamClass.create(to, (error, writeStream) => {
    if (error) {
      console.error(`${colors.red("[Error]")} ${error.message}`);
      return;
    }

    if (writeStream) {
      const from = options.from;

      if (typeof from !== "string") {
        console.error(`${colors.red("[Error]")} Reader option 'from' must be a string.`);
        return;
      }

      const readStream = fs.createReadStream(from);

      stream.pipeline(
        readStream,
        writeStream,
        (lineError) => {
          if (lineError) {
            console.error(`${colors.red("[Error]")} ${lineError.message}`);
            return;
          }

          console.log(`${colors.green("[Success]")} Uploading information to remote machine was finished.`);
        }
      );
    }
  });
}

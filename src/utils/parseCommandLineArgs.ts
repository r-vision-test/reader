/**
 * Обрабатываем массив с аргументами командной строки и возвращает объект с опциями сервиса.
 * @param argv Массив с настройками сервиса, по умолчанию - аргументы командной строки.
 * @returns Опции сервиса.
 */
export default function parseCommandLineArgs(argv: string[] = process.argv): IReaderOptions {
  const options: IReaderOptions = {};

  for (let index = 0; index <= argv.length; index++) {
    const current = argv[index];
    const next = argv[index + 1] || undefined;

    switch (current) {
      case "--type":
        options.type = next;
        index++;
        break;

      case "-ws":
      case "--web-socket":
        options.type = "web-socket";
        break;

      case "--nats":
        options.type = "nats";
        break;

      case "-f":
      case "--from":
        options.from = next;
        index++;
        break;

      case "-t":
      case "--to":
        options.to = next;
        index++;
        break;
    }
  }

  return options;
}

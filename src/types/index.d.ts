
/**
 * Опции Reader'а.
 */
declare interface IReaderOptions {

  /**
   * Способ передачи данных.
   */
  type?: "web-socket" | "nats" | string;

  /**
   * Расположение передаваемого файла на локальной машине.
   */
  from?: string;

  /**
   * Адрес удаленного ресурса в сети.
   */
  to?: string;
}

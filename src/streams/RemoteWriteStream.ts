import { Writable, WritableOptions } from "stream";
import { ConnectionStatus } from "./ConnectionStatus";
import { isNull } from "util";

/**
 * Поток записи на удаленный ресурс.
 */
export default abstract class RemoteWriteStream extends Writable {

  protected messages = 0;

  /**
   * Статус текущего подключения.
   */
  protected status: ConnectionStatus = ConnectionStatus.NotConnected;

  /**
   * Устанавливает подключение поток к удаленному ресурсу.
   * @param host Адрес удаленного ресурса.
   * @param callback Функция обратного вызова, которая вызывается в случае успешного подключения или ошибки.
   */
  protected abstract connect: (
    host: string,
    callback: (error?: Error | null) => void
  ) => void;

  /**
   * Отправляет данные на удаленные ресурс.
   * @param chunk Часть данных для передачи.
   * @param callback Функция обратного вызова, которая вызывается в случае успешной передачи данных или ошибки.
   */
  protected abstract send: (
    chunk: Buffer | string | undefined,
    callback: (error?: Error | null) => void
  ) => void;

  protected constructor(options?: WritableOptions) {
    super(options);
  }

  /**
   * Подключаемся к удаленному ресурсу и создает поток для записи.
   * @param host Адрес удаленного ресурса.
   * @param callback Функция обратного вызова, которая вызывается в случае ошибки или успешного подключения с передачей ссылки на поток.
   */
  public static create(
    this,
    host: string,
    callback: (error?: Error | null, stream?: RemoteWriteStream) => void
  ) {
    try {
      const stream = new this();

      stream.connect(host, connectError => {
        if (connectError) {
          callback(connectError);
          return;
        }

        callback(null, stream);
      });
    } catch (error) {
      callback(error);
    }
  }

  /**
   * Получает данные на запись и переправляет их на отправку на удаленный ресурс.
   * @param chunk Часть данных.
   * @param encoding Кодировка данных.
   * @param callback Функция обратного вызова, которая вызывается в случае успешной передачи данных или ошибки.
   */
  _write = (
    chunk: any,
    encoding: string,
    callback: (error?: Error | null) => void
  ): void => {
    if (this.status !== ConnectionStatus.Connected) {
      callback(
        new Error("Stream does not have active connection to remote machine")
      );
      return;
    }

    this.send(chunk, (error) => {
      if (error) {
        callback(error);
        return;
      }

      callback();
    });
  };

  /**
   * Завершает передачу данных.
   * @param callback Функция обратного вызова, которая вызывается в случае успешного завершения передачи данных или ошибки.
   */
  _final = (callback: (error?: Error | null) => void): void => {
    if (this.status !== ConnectionStatus.Connected) {
      callback(
        new Error(
          "Data transfer cannot be finalized, stream does not have active connection to remote machine"
        )
      );
      return;
    }

    this.status = ConnectionStatus.Closed;
    this.send(undefined, (error) => {
      if (error) {
        callback(error);
        return;
      }

      callback();
    });
  };

  /**
   * Переопределяем запись для возможности её остановки в случае команды со стороны удаленного ресурса.
   */
  write = (chunk, ...args): boolean => {
    return super.write(chunk, ...args);
  };
}

import { EventEmitter } from "events";
import NatsWriteStream from "./NatsWriteStream";

const hosts: { [host: string]: { incoming: EventEmitter; outgoing: EventEmitter } } = {};

const getEventEmitters = (host) => {
  if (typeof hosts[host] === "undefined") {
    hosts[host] = {
      incoming: new EventEmitter(),
      outgoing: new EventEmitter(),
    };
  }

  return hosts[host];
};

jest.mock("nats", () => ({
  connect: function (host: string) {
    if (host === "incorrect") {
      throw new Error("Could not connect to server: Error: getaddrinfo ENOTFOUND incorrect incorrect:4222");
    }

    const emitters = getEventEmitters(host);

    return {
      on: function (event, listener) { emitters.incoming.on(event, listener); return this; },
      request: function (event, ...args: any[]) { emitters.outgoing.emit(event, ...args); return this; },
      flush: function (listener) { emitters.outgoing.emit("flush", listener); return this; },
      close: function (listener) { emitters.outgoing.emit("close", listener); return this; },
    };
  },
}));

it("Should throw error due to undefined host", done => {
  NatsWriteStream.create(undefined as any, (error) => {
    expect(error?.message).toBe("NATS host must be a string");
    done();
  });
});

it("Should throw error due to incorrect host", done => {
  NatsWriteStream.create("incorrect", (error) => {
    expect(error?.message).toBe("Could not connect to server: Error: getaddrinfo ENOTFOUND incorrect incorrect:4222");
    done();
  });
});

it("Should throw error while connecting to remote machine", done => {
  const host = "nats://localhost:4222/";
  const emitters = getEventEmitters(host);

  NatsWriteStream.create(host, (error) => {
    expect(error?.message).toBe("Error while connecting to remote machine");
    done();
  });

  emitters.incoming.emit("error", new Error("Error while connecting to remote machine"));
});

it("Should end write to remote machine", done => {
  const host = "nats://localhost:4223/";
  const emitters = getEventEmitters(host);

  emitters.outgoing.on("transferring", (chunk, listener) => {
    listener();
  });

  emitters.outgoing.on("flush", listener => listener());
  emitters.outgoing.on("close", listener => done());

  NatsWriteStream.create(host, (error, stream) => {
    stream?.end((finalError) => undefined);
  });

  emitters.incoming.emit("connect");
});

it("Should not send data after closing connection", done => {
  const host = "nats://localhost:4224/";
  const emitters = getEventEmitters(host);

  NatsWriteStream.create(host, (createError, stream) => {
    stream?.on("close", () => {
      stream._write("", "", (error) => {
        expect(error?.message).toBe("Stream does not have active connection to remote machine");
        done();
      });
    });
    emitters.incoming.emit("close");
  });

  emitters.incoming.emit("connect");
});

it("Should destroy stream due to NATS error", done => {
  const host = "nats://localhost:4225/";
  const emitters = getEventEmitters(host);

  NatsWriteStream.create(host, (error, stream) => {
    stream?.on("error", (NatsError) => {
      expect(NatsError.message).toBe("NATS error");
      done();
    });

    emitters.incoming.emit("error", new Error("NATS error"));
  });

  emitters.incoming.emit("connect");
});

it("Should destroy stream due to NATS disconnecting", done => {
  const host = "nats://localhost:4226/";
  const emitters = getEventEmitters(host);

  NatsWriteStream.create(host, (error, stream) => {
    stream?.on("error", (NatsError) => {
      expect(NatsError.message).toBe("NATS disconnect");
      done();
    });

    emitters.incoming.emit("disconnect");
  });

  emitters.incoming.emit("connect");
});

it("Should successfully write data to remote machine", done => {
  const host = "nats://localhost:4227/";
  const emitters = getEventEmitters(host);

  const buffer = Buffer.from("Test buffer.", "utf8");

  emitters.outgoing.on("transferring", (chunk, listener) => {
    expect(buffer.toString("base64")).toBe(chunk);
    done();
  });

  NatsWriteStream.create(host, (error, stream) => {
    stream?._write(buffer, "buffer", (writeError) => undefined);
  });

  emitters.incoming.emit("connect");
});

afterAll(() => {
  jest.clearAllMocks();
});

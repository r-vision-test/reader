import { EventEmitter } from "events";
import WebSocketWriteStream from "./WebSocketWriteStream";

const hosts: { [host: string]: { incoming: EventEmitter; outgoing: EventEmitter } } = {};

const getEventEmitters = (host) => {
  if (typeof hosts[host] === "undefined") {
    hosts[host] = {
      incoming: new EventEmitter(),
      outgoing: new EventEmitter(),
    };
  }

  return hosts[host];
};

jest.mock("ws", () => {
  return jest.fn().mockImplementation((host: string) => {
    if (host === "incorrect") {
      throw new Error("Invalid URL: incorrect");
    }

    const emitters = getEventEmitters(host);

    return {
      on: function (event, listener) { emitters.incoming.on(event, listener); return this; },
      send: function (...args: any[]) { emitters.outgoing.emit("message", ...args); return this; },
    };
  });
});

it("Should throw error due to undefined host", done => {
  WebSocketWriteStream.create(undefined as any, (error) => {
    expect(error?.message).toBe("WebSocket host must be a string");
    done();
  });
});

it("Should throw error due to incorrect host", done => {
  WebSocketWriteStream.create("incorrect", (error) => {
    expect(error?.message).toBe("Invalid URL: incorrect");
    done();
  });
});

it("Should throw error while connecting to remote machine", done => {
  const host = "ws://localhost:1001/";
  const emitters = getEventEmitters(host);

  WebSocketWriteStream.create(host, (error) => {
    expect(error?.message).toBe("Error while connecting to remote machine");
    done();
  });

  emitters.incoming.emit("error", new Error("Error while connecting to remote machine"));
});

it("Should end write to remote machine", done => {
  const host = "ws://localhost:1002/";
  const emitters = getEventEmitters(host);

  emitters.outgoing.on("message", (chunk, listener) => {
    listener();
    emitters.incoming.emit("message", "reply:1");
  });

  WebSocketWriteStream.create(host, (error, stream) => {
    stream?.end((finalError) => {
      done();
    });
  });

  emitters.incoming.emit("open");
});

it("Should not send data after closing connection", done => {
  const host = "ws://localhost:1003/";
  const emitters = getEventEmitters(host);

  WebSocketWriteStream.create(host, (createError, stream) => {
    stream?.on("close", () => {
      stream._write("", "", (error) => {
        expect(error?.message).toBe("Stream does not have active connection to remote machine");
        done();
      });
    });
    emitters.incoming.emit("close", 1001);
  });

  emitters.incoming.emit("open");
});

it("Should throw error while sending data", done => {
  const host = "ws://localhost:1004/";
  const emitters = getEventEmitters(host);

  emitters.outgoing.on("message", (chunk, listener) => {
    listener(new Error("Error while sending data"));
  });


  WebSocketWriteStream.create(host, (createError, stream) => {
    stream?.on("error", (error) => {
      expect(error?.message).toBe("Error while sending data");
      done();
    });

    stream?.write(Buffer.from("QWERTY"));
  });

  emitters.incoming.emit("open");
});

it("Should destroy stream due to WebSocket error", done => {
  const host = "ws://localhost:1005/";
  const emitters = getEventEmitters(host);

  emitters.outgoing.on("message", (chunk, listener) => {
    listener();
  });

  WebSocketWriteStream.create(host, (error, stream) => {
    stream?.on("error", (webSocketError) => {
      expect(webSocketError.message).toBe("WebSocket error");
      done();
    });

    emitters.incoming.emit("error", new Error("WebSocket error"));
  });

  emitters.incoming.emit("open");
});

it("Should successfully write data to remote machine", done => {
  const host = "ws://localhost:1006/";
  const emitters = getEventEmitters(host);

  emitters.outgoing.on("message", (chunk, listener) => {
    listener();
    emitters.incoming.emit("message", "reply:1");
  });

  WebSocketWriteStream.create(host, (error, stream) => {
    stream?._write(Buffer.from("Test buffer.", "utf8"), "buffer", (writeError) => {
      if (writeError) {
        return;
      }

      done();
    });
  });

  emitters.incoming.emit("open");
});

it("Should finalize writing data to remote machine", done => {
  const host = "ws://localhost:1006/";
  const emitters = getEventEmitters(host);

  emitters.outgoing.on("message", (chunk, listener) => {
    listener();
    emitters.incoming.emit("message", "reply:1");
  });

  WebSocketWriteStream.create(host, (error, stream) => {
    stream?._final((finalError) => {
      if (finalError) {
        return;
      }

      done();
    });
  });

  emitters.incoming.emit("open");
});

it("Should throw error due to finalize writing data to remote machine", done => {
  const host = "ws://localhost:1007/";
  const emitters = getEventEmitters(host);

  WebSocketWriteStream.create(host, (createError, stream) => {
    stream?.on("close", () => {
      stream._final((error) => {
        expect(error?.message).toBe("Data transfer cannot be finalized, stream does not have active connection to remote machine");
        done();
      });
    });
    emitters.incoming.emit("close", 1001);
  });

  emitters.incoming.emit("open");
});

it("Should throw error due to sending final message to remote machine", done => {
  const host = "ws://localhost:1008/";
  const emitters = getEventEmitters(host);

  emitters.outgoing.on("message", (chunk, listener) => {
    listener(new Error("Error due to sending final message"));
  });

  WebSocketWriteStream.create(host, (error, stream) => {
    stream?._final((finalError) => {
      expect(finalError?.message).toBe("Error due to sending final message");
      done();
    });
  });

  emitters.incoming.emit("open");
});

afterAll(() => {
  jest.clearAllMocks();
});

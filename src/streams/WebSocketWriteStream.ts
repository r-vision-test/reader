import * as WebSocket from "ws";
import RemoteWriteStream from "./RemoteWriteStream";
import { ConnectionStatus } from "./ConnectionStatus";
import { EventEmitter } from "events";

/**
 * Поток записи на удаленный ресурс по средствам вебсокетов.
 */
export default class WebSocketWriteStream extends RemoteWriteStream {

  /**
   * Вебсокет-клиент.
   */
  private webSocket: WebSocket;

  /**
   * События обработки ответа сервера на запись части данных.
   */
  private messageRepliesEmitter = new EventEmitter();

  /**
   * Идентификатор последнего отправленного сообщения.
   */
  private messageIds = 0;

  protected constructor() {
    super();
  }

  /**
   * Подключение к удаленному ресурсу и создание поток для записи.
   * @param host Адрес удаленного ресурса.
   * @param callback Функция обратного вызова, которая вызывается в случае ошибки или успешного подключения с передачей ссылки на поток.
   */
  public static create(
    this,
    host: string,
    callback: (error?: Error | null, stream?: RemoteWriteStream) => void
  ): void {
    super.create(host, callback);
  }

  /**
   * Устанавливает подключение к удаленному ресурсу по средствам вебсокета.
   * @param host Адрес удаленного ресурса.
   * @param callback Функция обратного вызова, которая вызывается в случае успешного подключения или ошибки.
   */
  protected connect = (
    host: string,
    callback: (error?: Error | null) => void
  ): void => {
    if (typeof host !== "string") {
      callback(
        new Error("WebSocket host must be a string")
      );
      return;
    }

    this.webSocket = new WebSocket(host);

    this.webSocket.on("error", error => {
      if (this.status === ConnectionStatus.NotConnected) {
        callback(error);
      } else {
        this.destroy(error);
      }

      this.status = ConnectionStatus.Error;
    });

    this.webSocket.on("open", () => {
      this.status = ConnectionStatus.Connected;
      callback();
    });

    this.webSocket.on("close", (code) => {
      this.destroy();
      this.status = ConnectionStatus.Closed;
    });

    this.webSocket.on("message", data => {
      if (typeof data === "string" && data.startsWith("reply:")) {
        this.messageRepliesEmitter.emit(data);
      }
    });
  };

  /**
   * Отправляет данные на удаленные ресурс.
   * @param chunk Часть данных для передачи.
   * @param callback Функция обратного вызова, которая вызывается в случае успешной передачи данных или ошибки.
   */
  protected send = (
    chunk: any,
    callback: (error?: Error | null) => void
  ): void => {
    this.webSocket.send(chunk, (error) => {
      if (error) {
        this.status = ConnectionStatus.Error;
        callback(error);
        return;
      }

      const eventName = `reply:${++this.messageIds}`;
      this.messageRepliesEmitter.once(eventName, () => {
        callback();
      });
    });
  };
}

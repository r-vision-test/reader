export enum ConnectionStatus {
  NotConnected,
  Connected,
  Closed,
  Error
}

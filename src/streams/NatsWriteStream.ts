import * as NATS from "nats";
import RemoteWriteStream from "./RemoteWriteStream";
import { ConnectionStatus } from "./ConnectionStatus";

/**
 * Поток записи на удаленный ресурс по средствам NATS.
 */
export default class NatsWriteStream extends RemoteWriteStream {

  /**
   * NATS клиент.
   */
  private nc: NATS.Client;

  protected constructor() {
    super();
  }

  /**
   * Подключение к удаленному ресурсу и создание поток для записи.
   * @param host Адрес удаленного ресурса.
   * @param callback Функция обратного вызова, которая вызывается в случае ошибки или успешного подключения с передачей ссылки на поток.
   */
  public static create(
    this,
    host: string,
    callback: (error?: Error | null, stream?: RemoteWriteStream) => void
  ): void {
    super.create(host, callback);
  }

  /**
   * Устанавливает подключение к удаленному ресурсу по средствам NATS.
   * @param host Адрес удаленного ресурса.
   * @param callback Функция обратного вызова, которая вызывается в случае успешного подключения или ошибки.
   */
  protected connect = (
    host: string,
    callback: (error?: Error | null) => void
  ): void => {
    if (typeof host !== "string") {
      callback(
        new Error("NATS host must be a string")
      );
      return;
    }

    this.nc = NATS.connect(host);

    this.nc.on("connect", () => {
      this.status = ConnectionStatus.Connected;
      callback();
    });

    this.nc.on("error", error => {
      if (this.status === ConnectionStatus.NotConnected) {
        callback(error);
      } else {
        this.destroy(error);
      }

      this.status = ConnectionStatus.Error;
    });

    this.nc.on("disconnect", () => {
      this.destroy(new Error("NATS disconnect"));
      this.status = ConnectionStatus.Closed;
    });

    this.nc.on("close", () => {
      this.destroy();
      this.status = ConnectionStatus.Closed;
    });

    this.on("finish", () => {
      this.nc.flush(() => {
        this.nc.close();
      });
    });
  };

  /**
   * Отправляет данные на удаленные ресурс.
   * @param chunk Часть данных для передачи.
   * @param callback Функция обратного вызова, которая вызывается в случае успешной передачи данных или ошибки.
   */
  protected send = (
    chunk: any,
    callback: (error?: Error | null) => void
  ): void => {
    if (typeof chunk !== "undefined") {
      chunk = (chunk as Buffer).toString("base64");
    } else {
      chunk = "";
    }

    this.nc.request("transferring", chunk, () => {
      callback();
    });
  };
}

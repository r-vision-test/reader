jest.mock("ws", () => {
  return jest.fn().mockImplementation((host: string) => {
    if (host === "incorrect") {
      throw new Error("Invalid URL: incorrect");
    }
    return {
      on: (event: string, listener: Function) => event === "open" && listener(),
    };
  });
});

import * as colors from "colors";
import reader from "./reader";

it("Should output error with empty options", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Reader option 'type' must be a string.`);
    done();
  });
  reader();
});

it("Should output error with unknown 'type' option", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Write stream is undefined.`);
    done();
  });
  reader({
    type: "rabbitmq",
  });
});

it("Should output error with undefined 'to' option", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Reader option 'to' must be a string.`);
    done();
  });
  reader({
    type: "nats",
  });
});

it("Should output error with invalid WebSocket url", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Invalid URL: incorrect`);
    done();
  });

  reader({
    type: "web-socket",
    to: "incorrect",
  });
});

it("Should output error with undefined 'from' option", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toBe(`${colors.red("[Error]")} Reader option 'from' must be a string.`);
    done();
  });

  reader({
    type: "web-socket",
    to: "ws://localhost:2222/",
  });
});

it("Should output error with incorrect 'from' option", done => {
  console.error = jest.fn((...args) => {
    expect(args[0]).toMatch(/.*\[Error].* ENOENT: no such file or directory/);
    done();
  });

  reader({
    type: "web-socket",
    to: "ws://localhost:2222/",
    from: "./incorrect",
  });
});

afterAll(() => {
  jest.clearAllMocks();
});
